#include "bullet.h"

Bullet::~Bullet() {}

Bullet::Bullet(){
    _vel    = GMlib::Vector<float,3>(0.0,0.0,-5.0);
    _time   = 0.0;
    _g      = GMlib::Vector<float,3>(0.0, 0.0, -9.81);
}


void Bullet::localSimulate(double dt){  // dt :time from system

    _time += dt;
    _vel  += _time*(this->getMatrixToSceneInverse()*_g);
    this->translate(_vel);

}




//g IN SCENE COORDINATE
//change g to global coordinate
