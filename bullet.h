#ifndef BULLET_H
#define BULLET_H

#include <QDebug>
#include <parametrics/gmpsphere>
#include <parametrics/gmpbeziersurf>

class Bullet : public GMlib::PSphere<float> {

private:

    GMlib::Vector<float,3>   _vel;         //velocity
    float                    _time;
    GMlib::Vector<float,3>   _g;

public:

     Bullet();
     ~Bullet() ;

protected:
    // dt :time from system
    void localSimulate(double dt) override;

}; // END class



#endif // BULLET_H
