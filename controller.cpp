#include "controller.h"
#include <parametrics/gmpbeziersurf>
#include <parametrics/gmpsurfpointsvisualizer>

#include <QDebug>

Controller::Controller(){

    this->toggleVisible();
    insertScene();
}


void Controller::localSimulate(double dt){
    check();

}

void Controller::insertScene(){

    _vis_bf         = new VisBattleField();
    _vis_par        = new VisParticles();
    _vis_par2       = new VisParticles2();

    _vis_dirlight   = new VisDirLight();
    _vis_dirlight2  = new VisDirLight();
    _vis_dirlight3  = new VisDirLight();

    _vis_pointlight = new VisPointLight() ;
    _vis_pointlight2= new VisPointLight() ;



    const GMlib::Point<float,3> p1( 0.0, 0.0, 0.0 );
    const GMlib::Point<float,3> p2( 3.0, 0.0, 0.0 );
    const GMlib::Point<float,3> p3( 0.0, 1.0, 0.0 );
    const GMlib::Point<float,3> p4( 0.0, 0.0, 3.0 );

    const GMlib::Vector<float,3> v1(3.0, 0.0, 0.0);
    const GMlib::Vector<float,3> v2(0.0, 1.0, 0.0);
    const GMlib::Vector<float,3> v3(0.0, 0.0, 3.0);

    // _front_box - central
    _front_box = new TestPlane(p1,v3,v1);
    _front_box->insertVisualizer(_vis_dirlight);
    _front_box->rotate(180,GMlib::Point<float,3>( 0.0, 0.0, 1.0 )); //de huong mat ra sau
    _front_box->replot(20,20,1,1);
    this->insert(_front_box);

    // plane to sit on
    _plane = new TestCylinder(1.25,1.25,6.0); //rx,ry,h
    _plane->insertVisualizer(_vis_pointlight2);
    //  _plane->toggleDefaultVisualizer();

    _plane->replot(20,20,1,1);
    _plane->setMaterial(GMlib::GMmaterial::Ruby);
    _plane->rotate(90,GMlib::Point<float,3>( 1.0, 0.0, 0.0 ));
    _plane->translate(GMlib::Vector<float,3>(1.5,-2.0,-1.5));
    _front_box->insert(_plane);

    _ahead = new TestSphere(1.25,2,2);
    _ahead->toggleDefaultVisualizer();//de xuat hien ball
    _ahead->replot(20,20,1,1);
    _ahead->setMaterial(GMlib::GMmaterial::Ruby); //nho them truoc insert
    _ahead->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);
    _ahead->translate(GMlib::Vector<float,3>(0.0, 0.0, 2.5));
    _plane->insert(_ahead);

    _tail = new TestPlane(p1,v2,v3);
    _tail->toggleDefaultVisualizer();
    _tail->replot(20,20,1,1);
    _tail->setMaterial(GMlib::GMmaterial::BlackPlastic);
    _tail->translate(GMlib::Vector<float,3>(0.0, 1.0, -3.0));
    _plane->insert(_tail);

    _tail2 = new TestPlane(p1,v2,v3);
    _tail2->toggleDefaultVisualizer();
    _tail2->replot(20,20,1,1);
    _tail2->setMaterial(GMlib::GMmaterial::BlackPlastic);
    _tail2->translate(GMlib::Vector<float,3>(0.0, 2.0, -3.5));
    _plane->insert(_tail2);

    _tail3 = new TestPlane(p1,v2,v3);
    _tail3->toggleDefaultVisualizer();
    _tail3->replot(20,20,1,1);
    _tail3->setMaterial(GMlib::GMmaterial::BlackPlastic);
    _tail3->translate(GMlib::Vector<float,3>(0.0, 3.0, -4.0));
    _plane->insert(_tail3);

    _left_wing = new TestPlane(p1,v1,v2);
    _left_wing->toggleDefaultVisualizer();
    _left_wing->replot(20,20,1,1);
    _left_wing->setMaterial(GMlib::GMmaterial::BlackPlastic);
    _left_wing->translate(GMlib::Vector<float,3>(-2.0, 0.0, 1.75));
    _left_wing->rotate(-25,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    _plane->insert(_left_wing);
    _left_wing2 = new TestPlane(p1,v1,v2);
    _left_wing2->toggleDefaultVisualizer();
    _left_wing2->replot(20,20,1,1);
    _left_wing2->setMaterial(GMlib::GMmaterial::BlackPlastic);
    _left_wing2->translate(GMlib::Vector<float,3>(0.0, 0.0, 0.0));
    _left_wing2->rotate(115,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    _left_wing->insert(_left_wing2);

    _right_wing = new TestPlane(p1,v1,v2);
    _right_wing->toggleDefaultVisualizer();
    _right_wing->replot(20,20,1,1);
    _right_wing->setMaterial(GMlib::GMmaterial::BlackPlastic);
    _right_wing->translate(GMlib::Vector<float,3>(-0.5, 0.0, 3.0));
    _right_wing->rotate(25,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    _plane->insert(_right_wing);

    _right_wing2 = new TestPlane(p1,v1,v2);
    _right_wing2->toggleDefaultVisualizer();
    _right_wing2->replot(20,20,1,1);
    _right_wing2->setMaterial(GMlib::GMmaterial::BlackPlastic);
    _right_wing2->translate(GMlib::Vector<float,3>(4.25, -0.05, -2.70));
    _right_wing2->rotate(-115,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    _right_wing->insert(_right_wing2);

    //_head
    _head = new TestSphere(1,2,2);
    //_head->toggleDefaultVisualizer();//de xuat hien ball

    _head->insertVisualizer(_vis_pointlight);

    _head->replot(20,20,1,1);
    _head->setMaterial(GMlib::GMmaterial::Silver); //nho them truoc insert
    // _head->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);
    _head->translate(GMlib::Vector<float,3>(1.3, 0.5, 4.0));
    _front_box->insert(_head);

    /*
    // eye

       const GMlib::Vector<float,3> m1(0.4, 0.0, 0.0);
        const GMlib::Vector<float,3> m2(0.0, 0.0, 0.4);
        left_eye = new TestPlane(p1,m2,m1);
        left_eye->toggleDefaultVisualizer();
        left_eye->replot(20,20,1,1);
        left_eye->setMaterial(GMlib::GMmaterial::BlackPlastic);
        left_eye->translate(GMlib::Vector<float,3>(-0.65,-0.75,0.5));

        _head->insert(left_eye);

        right_eye = new TestPlane(p1,m2,m1);
        right_eye->toggleDefaultVisualizer();
        right_eye->replot(20,20,1,1);
        right_eye->setMaterial(GMlib::GMmaterial::BlackPlastic);
        right_eye->translate(GMlib::Vector<float,3>(0.25,-0.8,0.5));
        _head->insert(right_eye);

        //mouth
        mouth = new TestPlane(p1,m2,m1);
        mouth->toggleDefaultVisualizer();
        mouth->replot(20,20,1,1);
        mouth->setMaterial(GMlib::GMmaterial::Snow);
        mouth->translate(GMlib::Vector<float,3>(-0.25,-1.0,0.0));
       _head->insert(mouth);
     */

    //2 hands
    _upper_lhand = new TestCylinder(0.3,0.3,1.4);
    _upper_lhand->toggleDefaultVisualizer();//de xuat hien ball
    _upper_lhand->replot(20,20,1,1);
    _upper_lhand->setMaterial(GMlib::GMmaterial::Silver);
    _upper_lhand->translate(GMlib::Vector<float,3>(0.0, 0.5, 2.0));
    _upper_lhand->rotate(30,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    _front_box->insert( _upper_lhand);


    _middle_lhand = new TestSphere(0.30,0.5,0.5);
    _middle_lhand->toggleDefaultVisualizer();//de xuat hien ball
    _middle_lhand->replot(20,20,1,1);
    _middle_lhand->setMaterial(GMlib::GMmaterial::Brass);
    _middle_lhand->translate(GMlib::Vector<float,3>(0.0, 0.05, -0.80));
    _upper_lhand->insert( _middle_lhand);

    _lower_lhand = new TestCylinder(0.3,0.3,1.0);
    _lower_lhand->toggleDefaultVisualizer();//de xuat hien ball
    _lower_lhand->replot(20,20,1,1);
    _lower_lhand->setMaterial(GMlib::GMmaterial::Silver);
    _lower_lhand->translate(GMlib::Vector<float,3>(0.20, 0.0, -0.45));// x: trai,phai,z:trenduoi, y truoc sau
    _lower_lhand->rotate(-30,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    _middle_lhand->insert( _lower_lhand);

    _gun_part1 = new TestPlane(p1,v1,v2);
    _gun_part1->toggleDefaultVisualizer();
    _gun_part1->replot(20,20,1,1);
    _gun_part1->setMaterial(GMlib::GMmaterial::Emerald);
    _gun_part1->translate(GMlib::Vector<float,3>(-0.70, -0.25, -0.5));
    _gun_part1->scale(GMlib::Point<float,3>( 0.5, 0.5, 0.5 ));
    _lower_lhand->insert(_gun_part1);

    _gun_part2 = new TestCylinder(0.3,0.3,0.2);
    _gun_part2->toggleDefaultVisualizer();
    _gun_part2->replot(20,20,1,1);
    _gun_part2->setMaterial(GMlib::GMmaterial::Emerald);
    _gun_part2->translate(GMlib::Vector<float,3>(0.75, 0.25, 0.0));
    _gun_part1->insert(_gun_part2);

    _upper_rhand = new TestCylinder(0.3,0.3,1.4);//can sua 1.0 thanh 1.4
    _upper_rhand->toggleDefaultVisualizer();//de xuat hien ball
    _upper_rhand->replot(20,20,1,1);
    _upper_rhand->translate(GMlib::Vector<float,3>(3.0, 0.5, 2.25));
    _upper_rhand->setMaterial(GMlib::GMmaterial::Silver);
    _upper_rhand->rotate(-30,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    _front_box->insert(_upper_rhand);

    _middle_rhand = new TestSphere(0.3,0.5,0.5);
    _middle_rhand->toggleDefaultVisualizer();//de xuat hien ball
    _middle_rhand->replot(20,20,1,1);
    _middle_rhand->setMaterial(GMlib::GMmaterial::Brass);
    _middle_rhand->translate(GMlib::Vector<float,3>(0.0, 0.05, -0.80));
    _upper_rhand->insert( _middle_rhand);

    _lower_rhand = new TestCylinder(0.3,0.3,1.0);
    _lower_rhand->toggleDefaultVisualizer();//de xuat hien ball
    _lower_rhand->replot(20,20,1,1);
    _lower_rhand->setMaterial(GMlib::GMmaterial::Silver);
    _lower_rhand->translate(GMlib::Vector<float,3>(-0.25, 0.0, -0.50)); //z tren duoi
    _lower_rhand->rotate(30,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    _middle_rhand->insert( _lower_rhand);


    //body

    _left_box = new TestPlane(p1,v2,v3);
    _left_box->insertVisualizer(_vis_dirlight2);
    _left_box->replot(20,20,1,1);
    _front_box->insert(_left_box);


    _right_box = new TestPlane(p2,v3,v2);
    _right_box->insertVisualizer(_vis_dirlight3);
    _right_box->replot(20,20,1,1);
    _front_box->insert(_right_box);

    _behind_box = new TestPlane(p3,v1,v3);
    _behind_box->toggleDefaultVisualizer();
    _behind_box->replot(20,20,1,1);
    _behind_box->setMaterial(GMlib::GMmaterial::Silver);
    _front_box->insert(_behind_box);

    _up_box = new TestPlane(p4,v2,v1);
    _up_box->toggleDefaultVisualizer();
    _up_box->replot(20,20,1,1);
    _up_box->setMaterial(GMlib::GMmaterial::Silver);
    _front_box->insert(_up_box);

    _down_box = new TestPlane(p1,v1,v2);
    _down_box->toggleDefaultVisualizer();
    _down_box->replot(20,20,1,1);
    _down_box->setMaterial(GMlib::GMmaterial::Emerald);
    _front_box->insert(_down_box);

    //2 legs
    _upper_lleg = new TestCylinder(0.4,0.4,1.25);
    _upper_lleg->toggleDefaultVisualizer();//de xuat hien ball
    _upper_lleg->replot(20,20,1,1);
    _upper_lleg->setMaterial(GMlib::GMmaterial::Silver);
    _upper_lleg->translate(GMlib::Vector<float,3>(0.5, 0.5, -0.25));
    _upper_lleg->rotate(30,GMlib::Point<float,3>( -0.4, 5.0, 0.0 ));
    _front_box->insert(_upper_lleg);

    _middle_lleg = new TestSphere(0.40,0.5,0.5);
    _middle_lleg->toggleDefaultVisualizer();//de xuat hien ball
    _middle_lleg->replot(20,20,1,1);
    _middle_lleg->setMaterial(GMlib::GMmaterial::Brass);
    _middle_lleg->translate(GMlib::Vector<float,3>(0.0, 0.05, -0.80));
    _upper_lleg->insert( _middle_lleg);

    _lower_lleg = new TestCylinder(0.4,0.4,1.75);
    _lower_lleg->toggleDefaultVisualizer();//de xuat hien ball
    _lower_lleg->replot(20,20,1,1);
    _lower_lleg->setMaterial(GMlib::GMmaterial::Silver);
    _lower_lleg->translate(GMlib::Vector<float,3>(0.35, 0.05, -0.75));
    _lower_lleg->rotate(-25,GMlib::Point<float,3>( -0.4, 5.0, 0.0 ));
    _middle_lleg->insert(_lower_lleg);

    _upper_rleg = new TestCylinder(0.4,0.4,1.25);
    _upper_rleg->toggleDefaultVisualizer();//de xuat hien ball
    _upper_rleg->replot(20,20,1,1);
    _upper_rleg->setMaterial(GMlib::GMmaterial::Silver);
    _upper_rleg->translate(GMlib::Vector<float,3>(2.65, 0.5, -0.25));
    _upper_rleg->rotate(-30,GMlib::Point<float,3>( -0.4, 5.0, 0.0 ));
    _front_box->insert(_upper_rleg);

    _middle_rleg = new TestSphere(0.4,0.5,0.5);
    _middle_rleg->toggleDefaultVisualizer();//de xuat hien ball
    _middle_rleg->replot(20,20,1,1);
    _middle_rleg->setMaterial(GMlib::GMmaterial::Brass);
    _middle_rleg->translate(GMlib::Vector<float,3>(0.0, 0.05, -0.80));
    _upper_rleg->insert( _middle_rleg);


    _lower_rleg = new TestCylinder(0.4,0.4,1.75);
    _lower_rleg->toggleDefaultVisualizer();//de xuat hien ball
    _lower_rleg->replot(20,20,1,1);
    _lower_rleg->setMaterial(GMlib::GMmaterial::Silver);
    _lower_rleg->translate(GMlib::Vector<float,3>(-0.4, -0.05, -0.85));
    _lower_rleg->rotate(25,GMlib::Point<float,3>( -0.5, 5.0, 0.25 ));
    _middle_rleg->insert(_lower_rleg);

    const GMlib::Point<float,3> p11( -30.0, -80.0, -55.0 );
    const GMlib::Vector<float,3> v11(64.0, 0.0, 0.0);
    const GMlib::Vector<float,3> v21(0.0, 512.0, 0.0);

    _battlefield = new TestPlane(p11,v11,v21);
    // _battlefield->toggleDefaultVisualizer();
    _battlefield->insertVisualizer(_vis_bf);
    _battlefield->replot(20,20,1,1);
    this->insert(_battlefield);

    TestSphere* h= new TestSphere(5.0,5.0,5.0);
    //  h->insertVisualizer(_vis_pointlight);
    h->replot(20,20,1,1);
    h->translate(GMlib::Vector<float,3>(20.0, 50.0, 20.0));
    this->insert(h);


    TestCylinder* a = new TestCylinder(5.0,5.0,10.0); ;
    a->insertVisualizer(_vis_par);
    a->replot(20,20,1,1);
    a->setMaterial(GMlib::GMmaterial::Silver);
    a->translate(GMlib::Vector<float,3>(10.0, 10.0, 10.0));
    // a->rotate(25,GMlib::Point<float,3>( -0.5, 5.0, 0.25 ));
    this->insert(a);

    // show particles
    TestSphere* b= new TestSphere(5.0,5.0,5.0);
    b->insertVisualizer(_vis_par2);
    b->replot(20,20,1,1);
    b->translate(GMlib::Vector<float,3>(5.0, -30.0, 30.0));
    this->insert(b);



    // to show in the screen
    this->setSurroundingSphere(GMlib::Sphere<float,3>(100));

}

void Controller::shootBullet(){
    Bullet* bullet;
    bullet = new Bullet();
    bullet->setRadius(0.5);
    bullet->toggleDefaultVisualizer();
    bullet->replot(20,20,1,1);
    bullet->setMaterial(GMlib::GMmaterial::Gold);
    bullet->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);
    _gun_part2->insert(bullet);
    _bullets.push_back(bullet);

}
// check bullet and the plane

void Controller::check(){

    GMlib::Point<float,3> bf_pos;
    bf_pos = _battlefield->getGlobalPos();

    for(int i = 0; i < _bullets.size(); i++)
    {
        GMlib::Point<float,3> bl_pos;
        bl_pos = _bullets[i]->getGlobalPos();
        // std::cout<<"global pos of bullet "<<i<<" th: "<<bl_pos<<std::endl;
        //if(bl_pos(2)<-20.0)
        //{
            // _bullets[i]->setRadius(0.7);
            // _bullets[i]->setMaterial(GMlib::GMmaterial::Ruby);

       // }

        if( bl_pos(2)<-24.0)
        {
            _bullets.erase(_bullets.begin()+i);
            this->remove(_bullets[i]);
        }
    }
}



void Controller::moveHandUp(){
    _upper_lhand->rotate(-60,GMlib::Point<float,3>( 5.0, 0.0, 0.0 ));
    _upper_lhand->translate(GMlib::Vector<float,3>(-0.5, -0.3, -0.10));// Z:truoc sau,x:tren,duoi;y:trai,phai
    _upper_lhand->replot(20,20,1,0);

    _middle_lhand->translate(GMlib::Vector<float,3>(0.1, 0.0, 0.0));//y:tren duoi,x:trai phai
    _middle_lhand->replot(20,20,1,0);

    _lower_lhand->rotate(-90,GMlib::Vector<float,3>(0.0, 5.0, 5.0));
    _lower_lhand->translate(GMlib::Vector<float,3>(0.25, 0.25, -0.5)); //x:trai,phai , y:truoc,sau,z:tren,duoi
    _lower_lhand->replot(20,20,1,0);


}



void Controller::moveHandAround(){    //move from right to left

    _lower_lhand->rotate(10,GMlib::Point<float,3>( 0.0, 0.0,0.75 ),GMlib::Vector<float,3>(-1.0, 0.0, 0.0));
    _lower_lhand->replot(20,20,1,0);
}


void Controller::moveHandAround2(){  //move from left to right

    _lower_lhand->rotate(10,GMlib::Point<float,3>( 0.0, 0.0,0.75 ),GMlib::Vector<float,3>(1.0, 0.0, 0.0));
    _lower_lhand->replot(20,20,1,0);
}


void Controller::moveForward(){
    _front_box->translate(GMlib::Vector<float,3>(0.0, -1.0, 0.0));
    _front_box->replot(20,20,1,0);

}

void Controller::moveLeft(){
    _front_box->rotate(10,GMlib::Vector<float,3>(0.0, 0.0, 1.0));
    _front_box->replot(20,20,1,0);

}

void Controller::moveRight(){
    _front_box->rotate(-10,GMlib::Vector<float,3>(0.0, 0.0, 1.0));
    _front_box->replot(20,20,1,0);

}
