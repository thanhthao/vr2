#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "testtorus.h"
#include "testplane.h"
#include "testsphere.h"
#include "testcylinder.h"
#include "bullet.h"
#include "vis_battlefield.h"
#include "vis_particles.h"
#include "vis_particles2.h"
#include "vis_dirlight.h"
#include "vis_pointlight.h"
#include <gmParametricsModule>

// GMlib
#include <core/gmarray>
// Qt
#include <QDebug>
#include <stdexcept>
#include <iostream>

class Controller:public GMlib::SceneObject{
    GM_SCENEOBJECT(Controller)
    private:



    TestPlane* _front_box;
    TestPlane* _left_box;
    TestPlane* _right_box ;
    TestPlane* _behind_box;
    TestPlane* _up_box;
    TestPlane* _down_box;

    TestCylinder* _plane;
    TestSphere* _ahead;
    TestPlane* _tail;
    TestPlane* _tail2;
    TestPlane* _tail3;
    TestPlane* _left_wing;
    TestPlane* _left_wing2;
    TestPlane* _right_wing;
    TestPlane* _right_wing2;

    TestSphere* _head;
    TestPlane* _left_eye;
    TestPlane* _right_eye;
    TestPlane*  _mouth;

    TestCylinder* _upper_lhand;
    TestSphere*   _middle_lhand;
    TestCylinder* _lower_lhand;
    TestPlane*    _gun_part1;
    TestCylinder* _gun_part2;

    TestCylinder* _upper_rhand;
    TestSphere*   _middle_rhand;
    TestCylinder* _lower_rhand;

    TestCylinder* _upper_lleg;
    TestCylinder* _upper_rleg;
    TestSphere*   _middle_lleg;
    TestSphere*   _middle_rleg;
    TestCylinder* _lower_lleg;
    TestCylinder* _lower_rleg;

    TestPlane* _battlefield;
    VisBattleField* _vis_bf;
    VisParticles* _vis_par;
    VisParticles2* _vis_par2;

    VisDirLight* _vis_dirlight;
    VisDirLight* _vis_dirlight2;
    VisDirLight* _vis_dirlight3;
    VisPointLight* _vis_pointlight;
    VisPointLight* _vis_pointlight2;

   std::vector<Bullet*> _bullets;

public:

    Controller();
    void insertScene();
    void moveHandUp();
    void moveHandAround();
    void moveHandAround2();
    void moveForward();
    void moveLeft();
    void moveRight();
    void shootBullet();
    void check();



protected:

    void localSimulate(double dt);   //virtual function, inherit from SceneObject

};


#endif // CONTROLLER_H


