#include "robot.h"
#include "visualizer.h"
#include <parametrics/gmpbeziersurf>
#include <parametrics/gmpsurfpointsvisualizer>

#include <QDebug>

Robot::Robot(){

    this->toggleVisible();
    insertScene();
}


void Robot::localSimulate(double dt){

}

void Robot::insertScene(){

    //  vis = new Visualization();

    key_M=false;
    key_S=false;
    key_D=false;

    const GMlib::Point<float,3> p1( 0.0, 0.0, 0.0 );
    const GMlib::Point<float,3> p2( 3.0, 0.0, 0.0 );
    const GMlib::Point<float,3> p3( 0.0, 1.0, 0.0 );
    const GMlib::Point<float,3> p4( 0.0, 0.0, 2.0 );

    const GMlib::Vector<float,3> v1(3.0, 0.0, 0.0);
    const GMlib::Vector<float,3> v2(0.0, 1.0, 0.0);
    const GMlib::Vector<float,3> v3(0.0, 0.0, 3.0);


    // cho front_box lam trung tam de insert
    front_box = new TestPlane(p1,v3,v1);
    front_box->toggleDefaultVisualizer();
    //  front_box->insertVisualizer(vis);
    front_box->replot(20,20,1,1);
    front_box->setMaterial(GMlib::GMmaterial::Brass);
    this->insert(front_box);
    // plane de ngoi
    plane = new TestCylinder(1.25,1.25,6.0); //rx,ry,h
    plane->toggleDefaultVisualizer();
    plane->replot(20,20,1,1);
    plane->setMaterial(GMlib::GMmaterial::Ruby);
    plane->rotate(90,GMlib::Point<float,3>( 5.0, 0.0, 0.0 ));
    plane->translate(GMlib::Vector<float,3>(1.5,-2.0,-1.5));
    front_box->insert(plane);

    ahead = new TestSphere(1.25,2,2);
    ahead->toggleDefaultVisualizer();//de xuat hien ball
    ahead->replot(20,20,1,1);
    ahead->setMaterial(GMlib::GMmaterial::Ruby); //nho them truoc insert
    ahead->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);
    ahead->translate(GMlib::Vector<float,3>(0.0, 0.0, 2.5));
    plane->insert(ahead);

    tail = new TestPlane(p1,v2,v3);
    tail->toggleDefaultVisualizer();
    tail->replot(20,20,1,1);
    tail->setMaterial(GMlib::GMmaterial::BlackPlastic);
    tail->translate(GMlib::Vector<float,3>(0.0, 1.0, -3.0));
    plane->insert(tail);

    tail2 = new TestPlane(p1,v2,v3);
    tail2->toggleDefaultVisualizer();
    tail2->replot(20,20,1,1);
    tail2->setMaterial(GMlib::GMmaterial::BlackPlastic);
    tail2->translate(GMlib::Vector<float,3>(0.0, 2.0, -3.5));
    plane->insert(tail2);

    tail3 = new TestPlane(p1,v2,v3);
    tail3->toggleDefaultVisualizer();
    tail3->replot(20,20,1,1);
    tail3->setMaterial(GMlib::GMmaterial::BlackPlastic);
    tail3->translate(GMlib::Vector<float,3>(0.0, 3.0, -4.0));
    plane->insert(tail3);

    left_wing = new TestPlane(p1,v1,v2);
    left_wing->toggleDefaultVisualizer();
    left_wing->replot(20,20,1,1);
    left_wing->setMaterial(GMlib::GMmaterial::BlackPlastic);
    left_wing->translate(GMlib::Vector<float,3>(-2.0, 0.0, 1.75));
    left_wing->rotate(-25,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    plane->insert(left_wing);
    left_wing2 = new TestPlane(p1,v1,v2);
    left_wing2->toggleDefaultVisualizer();
    left_wing2->replot(20,20,1,1);
    left_wing2->setMaterial(GMlib::GMmaterial::BlackPlastic);
    left_wing2->translate(GMlib::Vector<float,3>(0.0, 0.0, 0.0));
    left_wing2->rotate(115,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    left_wing->insert(left_wing2);

    right_wing = new TestPlane(p1,v1,v2);
    right_wing->toggleDefaultVisualizer();
    right_wing->replot(20,20,1,1);
    right_wing->setMaterial(GMlib::GMmaterial::BlackPlastic);
    right_wing->translate(GMlib::Vector<float,3>(-0.5, 0.0, 3.0));
    right_wing->rotate(25,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    plane->insert(right_wing);

    right_wing2 = new TestPlane(p1,v1,v2);
    right_wing2->toggleDefaultVisualizer();
    right_wing2->replot(20,20,1,1);
    right_wing2->setMaterial(GMlib::GMmaterial::BlackPlastic);
    right_wing2->translate(GMlib::Vector<float,3>(4.25, -0.05, -2.70));
    right_wing2->rotate(-115,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    right_wing->insert(right_wing2);

    //head
    head = new TestSphere(1,2,2);
    head->toggleDefaultVisualizer();//de xuat hien ball
    // try visualizer
    //head->insertVisualizer(vis);

    head->replot(20,20,1,1);
    head->setMaterial(GMlib::GMmaterial::Silver); //nho them truoc insert
    head->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);
    head->translate(GMlib::Vector<float,3>(1.3, 0.5, 4.0));
    front_box->insert(head);

    // eye

    //    left_eye = new TestSphere(0.15,0.25,0.25);
    //    left_eye->toggleDefaultVisualizer();//de xuat hien ball
    //    left_eye->replot(20,20,1,1);
    //    left_eye->setMaterial(GMlib::GMmaterial::BlackPlastic); //nho them truoc insert
    //    left_eye->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);
    //    left_eye->translate(GMlib::Vector<float,3>(-0.35,-0.5,0.75));
    //    head->insert(left_eye);

    //    right_eye = new TestSphere(0.15,0.25,0.25);
    //    right_eye->toggleDefaultVisualizer();//de xuat hien ball
    //    right_eye->replot(20,20,1,1);
    //    right_eye->setMaterial(GMlib::GMmaterial::BlackPlastic); //nho them truoc insert
    //    right_eye->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);
    //    right_eye->translate(GMlib::Vector<float,3>(0.40,-0.5,0.75));
    //    head->insert(right_eye);
    //    //mouth
    //    const GMlib::Vector<float,3> m1(0.5, 0.0, 0.0);
    //    const GMlib::Vector<float,3> m2(0.0, 0.0, 0.25);
    //    mouth = new TestPlane(p1,m2,m1);
    //    mouth->toggleDefaultVisualizer();
    //    //  front_box->insertVisualizer(vis);
    //    mouth->replot(20,20,1,1);
    //    mouth->setMaterial(GMlib::GMmaterial::Snow);
    //    mouth->translate(GMlib::Vector<float,3>(-0.25,-1.0,0.0));
    //    head->insert(mouth);

    //2 hands
    upper_lhand = new TestCylinder(0.3,0.3,1.4);
    upper_lhand->toggleDefaultVisualizer();//de xuat hien ball
    // hand_left->insertVisualizer(plane4_visualizer);
    upper_lhand->replot(20,20,1,1);
    upper_lhand->setMaterial(GMlib::GMmaterial::Silver);
    upper_lhand->translate(GMlib::Vector<float,3>(0.0, 0.5, 2.0));
    upper_lhand->rotate(30,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    front_box->insert( upper_lhand);


    middle_lhand = new TestSphere(0.30,0.5,0.5);
    middle_lhand->toggleDefaultVisualizer();//de xuat hien ball
    middle_lhand->replot(20,20,1,1);
    middle_lhand->setMaterial(GMlib::GMmaterial::Brass);
    middle_lhand->translate(GMlib::Vector<float,3>(0.0, 0.05, -0.80));
    upper_lhand->insert( middle_lhand);

    lower_lhand = new TestCylinder(0.3,0.3,1.0);
    lower_lhand->toggleDefaultVisualizer();//de xuat hien ball
    lower_lhand->replot(20,20,1,1);
    lower_lhand->setMaterial(GMlib::GMmaterial::Silver);
    lower_lhand->translate(GMlib::Vector<float,3>(0.30, 0.0, -1.25));
    lower_lhand->rotate(-30,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    upper_lhand->insert( lower_lhand);

    gun_part1 = new TestPlane(p1,v1,v2);
    gun_part1->toggleDefaultVisualizer();
    gun_part1->replot(20,20,1,1);
    gun_part1->setMaterial(GMlib::GMmaterial::Emerald);
    gun_part1->translate(GMlib::Vector<float,3>(0.25, -0.75, -0.5));
    gun_part1->rotate(90,GMlib::Point<float,3>( 0.0, 0.0, 5.0 ));
    gun_part1->scale(GMlib::Point<float,3>( 0.5, 0.5, 0.5 ));
    lower_lhand->insert(gun_part1);

    gun_part2 = new TestCylinder(0.3,0.3,0.2);
    gun_part2->toggleDefaultVisualizer();
    gun_part2->replot(20,20,1,1);
    gun_part2->setMaterial(GMlib::GMmaterial::Emerald);
    gun_part2->translate(GMlib::Vector<float,3>(0.75, 0.25, 0.0));
    gun_part1->insert(gun_part2);

    upper_rhand = new TestCylinder(0.3,0.3,1.4);//can sua 1.0 thanh 1.4
    upper_rhand->toggleDefaultVisualizer();//de xuat hien ball
    upper_rhand->replot(20,20,1,1);
    upper_rhand->translate(GMlib::Vector<float,3>(3.0, 0.5, 2.25));
    upper_rhand->setMaterial(GMlib::GMmaterial::Silver);
    upper_rhand->rotate(-30,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    front_box->insert(upper_rhand);

    middle_rhand = new TestSphere(0.3,0.5,0.5);
    middle_rhand->toggleDefaultVisualizer();//de xuat hien ball
    middle_rhand->replot(20,20,1,1);
    middle_rhand->setMaterial(GMlib::GMmaterial::Brass);
    middle_rhand->translate(GMlib::Vector<float,3>(0.0, 0.05, -0.80));
    upper_rhand->insert( middle_rhand);

    lower_rhand = new TestCylinder(0.3,0.3,1.0);
    lower_rhand->toggleDefaultVisualizer();//de xuat hien ball
    lower_rhand->replot(20,20,1,1);
    lower_rhand->setMaterial(GMlib::GMmaterial::Silver);
    lower_rhand->translate(GMlib::Vector<float,3>(-0.19, 0.0, -1.2));
    lower_rhand->rotate(30,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    upper_rhand->insert( lower_rhand);


    //body

    left_box = new TestPlane(p1,v2,v3);
    left_box->toggleDefaultVisualizer();
    // left_box->insertVisualizer(plane6_visualizer);
    left_box->replot(20,20,1,1);
    left_box->setMaterial(GMlib::GMmaterial::BlackPlastic);
    front_box->insert(left_box);


    right_box = new TestPlane(p2,v3,v2);
    right_box->toggleDefaultVisualizer();
    //right_box->insertVisualizer(plane7_visualizer);
    right_box->replot(20,20,1,1);
    right_box->setMaterial(GMlib::GMmaterial::Ruby);
    front_box->insert(right_box);

    behind_box = new TestPlane(p3,v1,v3);
    behind_box->toggleDefaultVisualizer();
    //behind_box->insertVisualizer(plane8_visualizer);
    behind_box->replot(20,20,1,1);
    behind_box->setMaterial(GMlib::GMmaterial::BlackRubber);
    front_box->insert(behind_box);

    up_box = new TestPlane(p4,v2,v1);
    up_box->toggleDefaultVisualizer();
    // up_box->insertVisualizer(plane9_visualizer);
    up_box->replot(20,20,1,1);
    up_box->setMaterial(GMlib::GMmaterial::Silver);
    front_box->insert(up_box);

    down_box = new TestPlane(p1,v1,v2);
    down_box->toggleDefaultVisualizer();
    //down_box->insertVisualizer(plane10_visualizer);
    down_box->replot(20,20,1,1);
    down_box->setMaterial(GMlib::GMmaterial::Emerald);
    front_box->insert(down_box);

    //2 legs
    upper_lleg = new TestCylinder(0.4,0.4,1.25);
    upper_lleg->toggleDefaultVisualizer();//de xuat hien ball
    upper_lleg->replot(20,20,1,1);
    upper_lleg->setMaterial(GMlib::GMmaterial::Silver);
    upper_lleg->translate(GMlib::Vector<float,3>(0.5, 0.5, -0.25));
    upper_lleg->rotate(30,GMlib::Point<float,3>( -0.4, 5.0, 0.0 ));
    front_box->insert(upper_lleg);

    lower_lleg = new TestCylinder(0.4,0.4,1.75);
    lower_lleg->toggleDefaultVisualizer();//de xuat hien ball
    lower_lleg->replot(20,20,1,1);
    lower_lleg->setMaterial(GMlib::GMmaterial::Silver);
    lower_lleg->translate(GMlib::Vector<float,3>(0.35, 0.05, -1.25));
    lower_lleg->rotate(-25,GMlib::Point<float,3>( -0.4, 5.0, 0.0 ));
    upper_lleg->insert(lower_lleg);

    //    TestCylinder* 1upper_rleg;
    //   1upper_rleg = new TestCylinder(0.4,0.4,1.25);
    //    1upper_rleg->toggleDefaultVisualizer();//de xuat hien ball
    //    1upper_rleg->replot(20,20,1,1);
    //    1upper_rleg->setMaterial(GMlib::GMmaterial::Silver);
    //    1upper_rleg->translate(GMlib::Vector<float,3>(2.65, 0.5, -0.25));

    upper_rleg = new TestCylinder(0.4,0.4,1.25);
    upper_rleg->toggleDefaultVisualizer();//de xuat hien ball
    upper_rleg->replot(20,20,1,1);
    upper_rleg->setMaterial(GMlib::GMmaterial::Silver);
    upper_rleg->translate(GMlib::Vector<float,3>(2.65, 0.5, -0.25));
    upper_rleg->rotate(-30,GMlib::Point<float,3>( -0.4, 5.0, 0.0 ));
    front_box->insert(upper_rleg);

    lower_rleg = new TestCylinder(0.4,0.4,1.75);
    lower_rleg->toggleDefaultVisualizer();//de xuat hien ball
    lower_rleg->replot(20,20,1,1);
    lower_rleg->setMaterial(GMlib::GMmaterial::Silver);
    lower_rleg->translate(GMlib::Vector<float,3>(-0.3, -0.05, -1.25));
    lower_rleg->rotate(25,GMlib::Point<float,3>( -0.4, 5.0, 0.0 ));
    upper_rleg->insert(lower_rleg);



    //skybox
    /* TestPlane* front_skybox;
    TestPlane* left_skybox;
    TestPlane* right_skybox ;
    TestPlane* behind_skybox;
    TestPlane* up_skybox;
    TestPlane* down_skybox;
    const GMlib::Point<float,3> s1( 0.0, 0.0, 0.0 );
    const GMlib::Point<float,3> s2( 300.0, 0.0, 0.0 );
    const GMlib::Point<float,3> s3( 0.0, 300.0, 0.0 );
    const GMlib::Point<float,3> s4( 0.0, 0.0, 300.0 );

    const GMlib::Vector<float,3> b1(300.0, 0.0, 0.0);
    const GMlib::Vector<float,3> b2(0.0, 300.0, 0.0);
    const GMlib::Vector<float,3> b3(0.0, 0.0, 300.0);


    // cho front_box lam trung tam de insert
    front_skybox = new TestPlane(s1,b3,b1);
    front_skybox->toggleDefaultVisualizer();
    front_skybox->insertVisualizer(vis);
    front_skybox->replot(20,20,1,1);
    front_skybox->setMaterial(GMlib::GMmaterial::Brass);
    front_skybox->translate(GMlib::Vector<float,3>(-10.0, -10.0, -10.0));

    this->insert(front_skybox);

    //body

    left_skybox = new TestPlane(s1,b2,b3);
    left_skybox->toggleDefaultVisualizer();
    left_skybox->replot(20,20,1,1);
    left_skybox->setMaterial(GMlib::GMmaterial::BlackPlastic);
    front_skybox->insert(left_skybox);


    right_skybox = new TestPlane(s2,b3,b2);
    right_skybox->toggleDefaultVisualizer();
    right_skybox->replot(20,20,1,1);
    right_skybox->setMaterial(GMlib::GMmaterial::Ruby);
    front_skybox->insert(right_skybox);

    behind_skybox = new TestPlane(s3,b1,b3);
    behind_skybox->toggleDefaultVisualizer();
    behind_skybox->replot(20,20,1,1);
    behind_skybox->setMaterial(GMlib::GMmaterial::BlackRubber);
    front_skybox->insert(behind_skybox);

    up_skybox = new TestPlane(s4,b2,b1);
    up_skybox->toggleDefaultVisualizer();
    up_skybox->replot(20,20,1,1);
    up_skybox->setMaterial(GMlib::GMmaterial::Silver);
    front_skybox->insert(up_skybox);

    down_skybox = new TestPlane(s1,b1,b2);
    down_skybox->toggleDefaultVisualizer();
    down_skybox->replot(20,20,1,1);
    down_skybox->setMaterial(GMlib::GMmaterial::Emerald);
    front_skybox->insert(down_skybox);

*/


    // de hien ra man hinh
    this->setSurroundingSphere(GMlib::Sphere<float,3>(100));

}

void Robot::shootBullet(){

    bullet = new Bullet(0.25,0.5,0.5);
    bullet->toggleDefaultVisualizer();//de xuat hien ball
    bullet->replot(20,20,1,1);
    bullet->setMaterial(GMlib::GMmaterial::Silver); //nho them truoc insert
    bullet->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);
    bullet->translate(GMlib::Vector<float,3>(0.0, 0.0, 0.0));
    lower_lhand->insert(bullet);

}

void Robot::moveHandUp(){
    upper_lhand->rotate(-60,GMlib::Point<float,3>( 5.0, 0.0, 0.0 ));
    upper_lhand->translate(GMlib::Vector<float,3>(-0.5, -0.3, -0.10));// Z:truoc sau,x:tren,duoi;y:trai,phai
    upper_lhand->replot(20,20,1,0);

    middle_lhand->translate(GMlib::Vector<float,3>(0.1, 0.0, 0.0));//y:tren duoi,x:trai phai
    middle_lhand->replot(20,20,1,0);

    lower_lhand->rotate(-90,GMlib::Vector<float,3>(0.0, 5.0, 5.0));
    lower_lhand->translate(GMlib::Vector<float,3>(0.25, 0.25, -0.5)); //x:trai,phai , y:truoc,sau,z:tren,duoi
     //lower_lhand->
    lower_lhand->replot(20,20,1,0);
}


void Robot::moveHandAround(){ //tu phai sang trai

    // lower_lhand->rotate(10,GMlib::Vector<float,3>(0.0, 0.0, 0.1)); xoay vong vong
    lower_lhand->rotate(10,GMlib::Point<float,3>( 0.0, 0.0,0.75 ),GMlib::Vector<float,3>(-1.0, 0.0, 0.0));

    lower_lhand->replot(20,20,1,0);
}

void Robot::moveHandAround2(){ //tu trai sang phai

    lower_lhand->rotate(10,GMlib::Point<float,3>( 0.0, 0.0,0.75 ),GMlib::Vector<float,3>(1.0, 0.0, 0.0));

    lower_lhand->replot(20,20,1,0);
}

void Robot::moveHandDown(){

    upper_lhand->translate(GMlib::Vector<float,3>(0.0, 0.5, 2.0));
    upper_lhand->rotate(30,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
    upper_lhand->replot(20,20,1,0);
    middle_lhand->translate(GMlib::Vector<float,3>(0.0, 0.05, -0.80));
    middle_lhand->replot(20,20,1,0);

    lower_lhand->translate(GMlib::Vector<float,3>(0.30, 0.0, -1.25));
    lower_lhand->rotate(-30,GMlib::Point<float,3>( 0.0, 5.0, 0.0 ));
   lower_lhand->replot(20,20,1,0);

}


void Robot::moveForward(){
    front_box->translate(GMlib::Vector<float,3>(0.0, -5.0, 0.0));
    front_box->replot(20,20,1,0);


}

void Robot::moveLeft(){
    front_box->rotate(10,GMlib::Vector<float,3>(0.0, 0.0, 0.1));
    front_box->replot(20,20,1,0);

}

void Robot::moveRight(){
    front_box->rotate(-10,GMlib::Vector<float,3>(0.0, 0.0, 0.1));
    front_box->replot(20,20,1,0);

}
