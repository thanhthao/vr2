#ifndef ROBOT_H
#define ROBOT_H

#include "testtorus.h"
#include "testplane.h"
#include "testsphere.h"
#include "testcylinder.h"
#include "bullet.h"
#include "visualizer.h"

#include <gmParametricsModule>

// GMlib
#include <core/gmarray>
// Qt
#include <QDebug>
#include <stdexcept>
#include <iostream>

//class Visualization;

class Robot:public GMlib::SceneObject{
    GM_SCENEOBJECT(Robot)
    private:

    TestPlane* front_box;
    TestPlane* left_box;
    TestPlane* right_box ;
    TestPlane* behind_box;
    TestPlane* up_box;
    TestPlane* down_box;

    TestCylinder* plane;
    TestSphere* ahead;
    TestPlane* tail;
    TestPlane* tail2;
    TestPlane* tail3;
    TestPlane* left_wing;
    TestPlane* left_wing2;
    TestPlane* right_wing;
    TestPlane* right_wing2;

    TestSphere* head;
    TestSphere* left_eye;
    TestSphere* right_eye;
    TestPlane*  mouth;

    TestCylinder* upper_lhand;
    TestSphere*   middle_lhand;
    TestCylinder* lower_lhand;
    TestPlane*    gun_part1;
    TestCylinder* gun_part2;

    TestCylinder* upper_rhand;
    TestSphere*   middle_rhand;
    TestCylinder* lower_rhand;

    TestCylinder* upper_lleg;
    TestCylinder* upper_rleg;
    TestCylinder* lower_lleg;
    TestCylinder* lower_rleg;
    Bullet* bullet;
    bool key_M;
    bool key_S;
    bool key_D;
    Visualization *vis;

public:

    Robot();
    void insertScene();
    void moveHandUp();
    void moveHandAround();
    void moveHandAround2();
    void moveHandDown();  //check later
    void moveForward();
    void moveLeft();
    void moveRight();
    void shootBullet();
    TestCylinder* getLowerLeftHand();


protected:

    void localSimulate(double dt);   //virtual function, inherit from SceneObject

};


#endif // ROBOT_H


