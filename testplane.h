#ifndef PLANE_H
#define PLANE_H


#include <parametrics/gmpplane>


class TestPlane : public GMlib::PPlane<float> {

private:

public:
  using PPlane::PPlane;
     ~TestPlane() { }

GMlib::Point<float,3> &getCornerPoint(){
    return this->_pt;  // Point<float,3> _pt in PPlane class
}


}; // END class TestPlane



#endif // TESTPLANE_H
