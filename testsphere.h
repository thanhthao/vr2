#ifndef HEAD_H
#define HEAD_H

#include <QDebug>
#include <parametrics/gmpsphere>
#include <parametrics/gmpbeziersurf>

class TestSphere : public GMlib::PSphere<float> {

private:


public:
    using PSphere::PSphere;
    ~TestSphere() { }
protected:
    // dt :time from system
    void localSimulate(double dt) override{}

}; // END class Ball



#endif // HEAD_H
