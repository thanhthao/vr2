
#include "vis_battlefield.h"
#include <SOIL.h>
// gmlib
#include <opengl/gmopengl.h>
#include <opengl/gmopenglmanager.h>
#include <scene/gmscene.h>
#include <scene/camera/gmcamera.h>
#include <scene/light/gmlight.h>
#include <scene/utils/gmmaterial.h>
#include <parametrics/gmpsurf.h>
#include <opengl/gmgeometryshader>
// stl
#include <set>
#include <string>


VisBattleField::VisBattleField()
    : _no_strips(0), _no_strip_indices(0), _strip_size(0)
{
    int img_height1;
    int img_width1;

    int img_height2;
    int img_width2;

    int img_height3;
    int img_width3;

    initShaderProgram();

    _color_prog.acquire("color");
    assert(_color_prog.isValid());

    _vbo.create();
    _vbo2.create();

    _ibo.create();


    glGenTextures(1, &_color_tex);
    glBindTexture(GL_TEXTURE_2D,_color_tex);
    unsigned char* img1 = SOIL_load_image("H:/subjects/virtualReality2/gmlib_qmldemo_01/images/grass.jpg", &img_width1, &img_height1,0,SOIL_LOAD_RGB);

    if(img1 == 0)
        std::cout<<" can not load the picture 1 for battlefield"<<std::endl;
    else
        std::cout<<" load the picture 1 for battlefield ok"<<std::endl;


    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA_FLOAT32_ATI, img_width1,
                 img_height1, 0, GL_RGB, GL_UNSIGNED_BYTE, img1);



    //height
    glGenTextures(1, &_height_tex);
    glBindTexture(GL_TEXTURE_2D,_height_tex);
    unsigned char* img2 = SOIL_load_image("H:/subjects/virtualReality2/gmlib_qmldemo_01/images/mountains2.png", &img_width2, &img_height2,0,SOIL_LOAD_RGB);

    if(img2 == 0)
        std::cout<<" can not load the picture 2 for battlefield"<<std::endl;
    else
        std::cout<<" load the picture 2 for battlefield ok"<<std::endl;

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA_FLOAT32_ATI, img_width2,
                 img_height2, 0, GL_RGB, GL_UNSIGNED_BYTE, img2);

    //light
    glGenTextures(1, &_light_tex);
    glBindTexture(GL_TEXTURE_2D,_light_tex);
    unsigned char* img3 = SOIL_load_image("H:/subjects/virtualReality2/gmlib_qmldemo_01/images/lightMap2012.bmp", &img_width3, &img_height3,0,SOIL_LOAD_RGB);

    if(img3 == 0)
        std::cout<<" can not load the picture 3 for battlefield"<<std::endl;
    else
        std::cout<<" load the picture 3 for battlefield ok"<<std::endl;

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA_FLOAT32_ATI, img_width3,
                 img_height3, 0, GL_RGB, GL_UNSIGNED_BYTE, img3);



}

VisBattleField::VisBattleField(const VisBattleField &copy)
    :PSurfVisualizer<float,3>(copy), _no_strips(0), _no_strip_indices(0), _strip_size(0)
{

    initShaderProgram();

    _color_prog.acquire("color");
    assert(_color_prog.isValid());

    _vbo.create();
    _ibo.create();

}

void VisBattleField::getNormal(GMlib::GL::VertexBufferObject  &vbo,
                               const GMlib::DMatrix<GMlib::Vector<float, 3> >  &n) {

    GLsizeiptr no_vertices = n.getDim1() * n.getDim2() * sizeof(GMlib::GL::GLNormal);

    vbo.bufferData( no_vertices, 0x0, GL_STATIC_DRAW );
    GMlib::GL::GLNormal *ptr = vbo.mapBuffer<GMlib::GL::GLNormal>();  //glnormal
    for( int i = 0; i < n.getDim1(); i++ ) {
        for( int j = 0; j < n.getDim2(); j++ ) {

            ptr->nx = n(i)(j)(0);
            ptr->ny = n(i)(j)(1);
            ptr->nz = n(i)(j)(2);

            ptr++;
        }
    }
    vbo.unmapBuffer();
}



void VisBattleField::render( const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer ) const {

    const GMlib::Camera* cam = renderer->getCamera();
    const GMlib::HqMatrix<float,3> &mvmat = obj->getModelViewMatrix(cam);
    const GMlib::HqMatrix<float,3> &pmat = obj->getProjectionMatrix(cam);

    GMlib::SqMatrix<float,3>        nmat = mvmat.getRotationMatrix();

    this->glSetDisplayMode();

    _prog.bind(); {
        _prog.setUniform( "u_mvmat", mvmat );
        _prog.setUniform( "u_mvpmat", pmat * mvmat );
        _prog.setUniform( "u_nmat", nmat );

        // Get vertex and texture attrib locations
        GMlib::GL::AttributeLocation vert_loc = _prog.getAttributeLocation( "in_vertices" );
        GMlib::GL::AttributeLocation tex_loc = _prog.getAttributeLocation( "in_texture" );
        GMlib::GL::AttributeLocation normal_loc = _prog.getAttributeLocation( "in_normal" );


        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D,_color_tex);

        //height
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D,_height_tex);

        //light
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D,_light_tex);


        GLuint _loc1 = _prog.getUniformLocation("_color_tex")();
        glUniform1i(_loc1,0); // send 0 because the texture is bound to GL_TEXTURE0
        //height
        GLuint _loc2 = _prog.getUniformLocation("_height_tex")();
        glUniform1i(_loc2,1); // send 1 because the texture is bound to GL_TEXTURE1

        //light
        GLuint _loc3 = _prog.getUniformLocation("_light_tex")();
        glUniform1i(_loc3,2);


        // Bind and draw
        _vbo.bind();
        _vbo.enable( vert_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );
        _vbo.enable( tex_loc,  2, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(3*sizeof(GLfloat)) );

        _vbo2.bind();
        _vbo2.enable( normal_loc,  3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLNormal), reinterpret_cast<const GLvoid *>(0x0) );
        //
        draw();

        _vbo2.disable( normal_loc );
        _vbo2.unbind();


        _vbo.disable( vert_loc );
        _vbo.disable( tex_loc );
        _vbo.unbind();

    } _prog.unbind();
}



void VisBattleField::draw() const {

    _ibo.bind();
    for( unsigned int i = 0; i < _no_strips; ++i )
        _ibo.drawElements( GL_TRIANGLE_STRIP, _no_strip_indices, GL_UNSIGNED_INT, reinterpret_cast<const GLvoid *>(i * _strip_size) );
    _ibo.unbind();
}



void VisBattleField::replot(
        const GMlib::DMatrix< GMlib::DMatrix< GMlib::Vector<float, 3> > >& p,
        const GMlib::DMatrix< GMlib::Vector<float, 3> >& normals,
        int /*m1*/, int /*m2*/, int /*d1*/, int /*d2*/,
        bool closed_u, bool closed_v  ) {

    PSurfVisualizer::fillStandardVBO( _vbo, p );
    PSurfVisualizer::fillTriangleStripIBO( _ibo, p.getDim1(), p.getDim2(), _no_strips, _no_strip_indices, _strip_size );
    // PSurfVisualizer::fillNMap( _nmap, normals, closed_u, closed_v );
    getNormal(_vbo2,normals);
}



void VisBattleField::renderGeometry( const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color ) const {

    _color_prog.bind(); {
        _color_prog.setUniform( "u_color", color );
        _color_prog.setUniform( "u_mvpmat", obj->getModelViewProjectionMatrix(renderer->getCamera()) );
        GMlib::GL::AttributeLocation vertice_loc = _color_prog.getAttributeLocation( "in_vertices" );

        _vbo.bind();
        _vbo.enable( vertice_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );

        draw();

        _vbo.disable( vertice_loc );
        _vbo.unbind();

    } _color_prog.unbind();
}


void VisBattleField::initShaderProgram() {

    const std::string prog_name    = "bf_prog2";
    if( _prog.acquire(prog_name) ) return;


    std::string vs_src =
            GMlib::GL::OpenGLManager::glslDefHeader150Source() +

            "in vec2 in_texture;\n"
            "in vec4 in_vertices,in_normal;\n"

            "uniform mat4 u_mvpmat, u_mvmat;\n"
            "uniform mat3 u_nmat;\n"
            "uniform sampler2D _height_tex;\n"
            "\n"

            "out vec4 gl_Position;\n"
            "out vec3 ex_normal;\n"
            "smooth out vec2 ex_texture;\n"
            "smooth out vec3 ex_vertices;\n"


            "\n"
            "void main(void) {\n"
            "\n"
            "  ex_texture = in_texture;\n"
            "  ex_normal = u_nmat*vec3(in_normal);\n"
            "  ex_vertices = vec3(u_mvmat*in_vertices).xyz;\n"

            " vec4 position = in_vertices ;\n "
            " position.z = texture(_height_tex,in_texture.xy).x*100.0-100.0;\n "
            " position.x *= 2.0;\n "

            "  gl_Position = u_mvpmat * position;"

            "}\n"
            ;



    std::string fs_src =
            GMlib::GL::OpenGLManager::glslDefHeader150Source() +


            "uniform sampler2D _color_tex,_light_tex;\n"
            "smooth in vec2 ex_texture;\n"
            "smooth in vec3 ex_normal;\n"
            "struct DirectionalLight \n"
            " {\n"
            "    vec3 vColor;\n"
            "    vec3 vDirection;\n"
            "    float fAmbient;\n"
            " };\n"


            " DirectionalLight sunLight= DirectionalLight(vec3(1.0,1.0,1.0),vec3(0.0,-1.0,0.0),0.25); \n"


            "\n"
            "void main(void) {\n"
            "\n"
            " vec4 outputColor = vec4(0.0, 0.0, 0.0, 0.0); \n"
            " vec4 vColor = vec4(1.0, 1.0, 1.0, 1.0); \n"

            "   for(int i = 0; i < 2; i++) \n"
            "   { \n"
            "      if(i == 0) \n"
            "      { \n"
            "        vec4 vTexColor = texture(_color_tex, ex_texture); \n"
            "        outputColor += vTexColor*0.7; \n"
            "     } \n"
            "     else \n"
            "     {\n"
            "           vec4 vTexColor = texture(_light_tex, ex_texture); \n"
            "           outputColor += vTexColor*0.3; \n"
            "     } \n"
            "  } \n"
            "  float fDiffuseIntensity = max(0.0, dot(normalize(ex_normal), -sunLight.vDirection)); \n"
            "   outputColor *= vColor*vec4(sunLight.vColor*(sunLight.fAmbient+fDiffuseIntensity), 1.0);\n"

            " gl_FragColor  = outputColor;\n"

            "}\n"
            ;



    bool compile_ok, link_ok;
    GMlib::GL::VertexShader vshader;
    vshader.create("texture_vs2");
    vshader.setPersistent(true);
    vshader.setSource(vs_src);
    compile_ok = vshader.compile();

    if( !compile_ok ) {
        std::cout << "Src:" << std::endl << vshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << vshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    GMlib::GL::FragmentShader fshader;
    fshader.create("texture_fs2");
    fshader.setPersistent(true);
    fshader.setSource(fs_src);
    compile_ok = fshader.compile();
    if( !compile_ok ) {
        std::cout << "Src:" << std::endl << fshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << fshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);


    _prog.create(prog_name);
    _prog.setPersistent(true);
    _prog.attachShader(vshader);
    _prog.attachShader(fshader);
    link_ok = _prog.link();
    if( !compile_ok ) {
        std::cout << "Src:" << std::endl << vshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << vshader.getCompilerLog() << std::endl;
    }
    assert(link_ok);
}




