
/**********************************************************************************
**
** Copyright (C) 1994 Narvik University College
** Contact: GMlib Online Portal at http://episteme.hin.no
**
** This file is part of the Geometric Modeling Library, GMlib.
**
** GMlib is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** GMlib is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with GMlib.  If not, see <http://www.gnu.org/licenses/>.
**
**********************************************************************************/
#include "vis_battlefield_2.h"
#include <SOIL.h>
// gmlib
#include <opengl/gmopengl.h>
#include <opengl/gmopenglmanager.h>
#include <scene/gmscene.h>
#include <scene/camera/gmcamera.h>
#include <scene/light/gmlight.h>
#include <scene/utils/gmmaterial.h>
#include <parametrics/gmpsurf.h>
// stl
#include <set>
#include <string>


VisBattleField2::VisBattleField2()
    : _no_strips(0), _no_strip_indices(0), _strip_size(0)
{
    int img_height1;
    int img_width1;
    // height
    int img_height2;
    int img_width2;

    initShaderProgram();

    _color_prog.acquire("color");  //phai color moi duoc
    assert(_color_prog.isValid());

    _vbo.create();
    _vbo2.create();

    _ibo.create();

    // moi them
    glGenTextures(1, &_color_tex);
    glBindTexture(GL_TEXTURE_2D,_color_tex);
    unsigned char* img1 = SOIL_load_image("H:/subjects/virtualReality2/gmlib_qmldemo_01/images/colorMap2012.bmp", &img_width1, &img_height1,0,SOIL_LOAD_RGB);

    if(img1 == 0)
        std::cout<<" img1=0,cant load the picture 1"<<std::endl;
    else
        std::cout<<" load picture 1 ok"<<std::endl;


    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA_FLOAT32_ATI, img_width1,
                 img_height1, 0, GL_RGB, GL_UNSIGNED_BYTE, img1);



    //height
    glGenTextures(1, &_height_tex);
    glBindTexture(GL_TEXTURE_2D,_height_tex);
    // unsigned char* img2 = SOIL_load_image("H:/subjects/virtualReality2/gmlib_qmldemo_01/images/heightMap2012.bmp", &img_width2, &img_height2,0,SOIL_LOAD_RGB);
    unsigned char* img2 = SOIL_load_image("H:/subjects/virtualReality2/gmlib_qmldemo_01/images/mountains2.png", &img_width2, &img_height2,0,SOIL_LOAD_RGB);

    if(img2 == 0)
        std::cout<<" img2=0"<<std::endl;
    else
        std::cout<<" load picture 2 ok"<<std::endl;


    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA_FLOAT32_ATI, img_width2,
                 img_height2, 0, GL_RGB, GL_UNSIGNED_BYTE, img2);




}

VisBattleField2::VisBattleField2(const VisBattleField2 &copy)
    :PSurfVisualizer<float,3>(copy), _no_strips(0), _no_strip_indices(0), _strip_size(0)
{

    initShaderProgram();

    _color_prog.acquire("color");
    assert(_color_prog.isValid());

    _vbo.create();
    _ibo.create();

}

void VisBattleField2::getNormal(GMlib::GL::VertexBufferObject  &vbo,
                             const GMlib::DMatrix<GMlib::Vector<float, 3> >  &n) {

  GLsizeiptr no_vertices = n.getDim1() * n.getDim2() * sizeof(GMlib::GL::GLNormal);

  vbo.bufferData( no_vertices, 0x0, GL_STATIC_DRAW );
  GMlib::GL::GLNormal *ptr = vbo.mapBuffer<GMlib::GL::GLNormal>();  //glnormal
  for( int i = 0; i < n.getDim1(); i++ ) {
    for( int j = 0; j < n.getDim2(); j++ ) {

      // vertex position //change to normal
      ptr->nx = n(i)(j)(0);
      ptr->ny = n(i)(j)(1);
      ptr->nz = n(i)(j)(2);

      ptr++;
    }
  }
  vbo.unmapBuffer();
}



void VisBattleField2::render( const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer ) const {

    const GMlib::Camera* cam = renderer->getCamera();
    const GMlib::HqMatrix<float,3> &mvmat = obj->getModelViewMatrix(cam);
    const GMlib::HqMatrix<float,3> &pmat = obj->getProjectionMatrix(cam);

     //  const SqMatrix<float,3> &nmat = obj->getNormalMatrix(cam);

    GMlib::SqMatrix<float,3>        nmat = mvmat.getRotationMatrix();



    this->glSetDisplayMode();

    _prog.bind(); {
        _prog.setUniform( "u_mvmat", mvmat );
        _prog.setUniform( "u_mvpmat", pmat * mvmat );
        _prog.setUniform( "u_nmat", nmat );

        // Get vertex and texture attrib locations
        GMlib::GL::AttributeLocation vert_loc = _prog.getAttributeLocation( "in_vertices" );
        GMlib::GL::AttributeLocation tex_loc = _prog.getAttributeLocation( "in_texture" );
        GMlib::GL::AttributeLocation normal_loc = _prog.getAttributeLocation( "in_normal" );


        glActiveTexture(GL_TEXTURE0);
        // glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,_color_tex);

        //height
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D,_height_tex);

        // glActiveTexture(GL_TEXTURE0);
        // glDisable(GL_TEXTURE_2D);
        GLuint loc = _prog.getUniformLocation("_color_tex")();
        glUniform1i(loc,0); // send 0 because the texture is bound to GL_TEXTURE0
        //height
        GLuint loc2 = _prog.getUniformLocation("_height_tex")();
        glUniform1i(loc2,1); // send 1 because the texture is bound to GL_TEXTURE1


        // Bind and draw
        _vbo.bind();
        _vbo.enable( vert_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );
        _vbo.enable( tex_loc,  2, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(3*sizeof(GLfloat)) );

        _vbo2.bind();
        _vbo2.enable( normal_loc,  3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLNormal), reinterpret_cast<const GLvoid *>(0x0) );
         //
         draw();

        _vbo2.disable( normal_loc );
        _vbo2.unbind();


        _vbo.disable( vert_loc );
        _vbo.disable( tex_loc );
        _vbo.unbind();

    } _prog.unbind();
}



void VisBattleField2::draw() const {

    _ibo.bind();
    for( unsigned int i = 0; i < _no_strips; ++i )
        _ibo.drawElements( GL_TRIANGLE_STRIP, _no_strip_indices, GL_UNSIGNED_INT, reinterpret_cast<const GLvoid *>(i * _strip_size) );
    _ibo.unbind();
}



void VisBattleField2::replot(
        const GMlib::DMatrix< GMlib::DMatrix< GMlib::Vector<float, 3> > >& p,
        const GMlib::DMatrix< GMlib::Vector<float, 3> >& normals,
        int /*m1*/, int /*m2*/, int /*d1*/, int /*d2*/,
        bool closed_u, bool closed_v  ) {

    PSurfVisualizer::fillStandardVBO( _vbo, p );
    PSurfVisualizer::fillTriangleStripIBO( _ibo, p.getDim1(), p.getDim2(), _no_strips, _no_strip_indices, _strip_size );
    // PSurfVisualizer::fillNMap( _nmap, normals, closed_u, closed_v );
    getNormal(_vbo2,normals);
}



void VisBattleField2::renderGeometry( const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color ) const {

    _color_prog.bind(); {
        _color_prog.setUniform( "u_color", color );
        _color_prog.setUniform( "u_mvpmat", obj->getModelViewProjectionMatrix(renderer->getCamera()) );
        GMlib::GL::AttributeLocation vertice_loc = _color_prog.getAttributeLocation( "in_vertices" );

        _vbo.bind();
        _vbo.enable( vertice_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );

        draw();

        _vbo.disable( vertice_loc );
        _vbo.unbind();

    } _color_prog.unbind();
}


void VisBattleField2::initShaderProgram() {

    const std::string prog_name    = "bf_prog2";
    if( _prog.acquire(prog_name) ) return;


    std::string vs_src =
            GMlib::GL::OpenGLManager::glslDefHeader150Source() +





            "in vec2 in_texture;\n"
            "in vec4 in_vertices,in_normal;\n"

            "uniform mat4 u_mvpmat,u_nmat;\n"
            "uniform sampler2D _height_tex;\n"
            "\n"
            "out vec4 gl_Position;\n"
            "out vec3 ex_normal;\n"
            "smooth out vec2 ex_texture;\n"
            "\n"
            "void main(void) {\n"
            "\n"
            "  ex_texture = in_texture;\n"
            "  ex_normal = (u_nmat*in_normal).xyz;\n"
            " vec4 position = in_vertices ;\n "
            " position.z = texture(_height_tex,in_texture.xy).x*100.0-100.0;\n "
            " position.x *= 2.0;\n "

            "  gl_Position = u_mvpmat * position;"

            "}\n"
            ;


    std::string fs_src =
            GMlib::GL::OpenGLManager::glslDefHeader150Source() +
            // GMlib::GL::OpenGLManager::glslFnComputeLightingSource() +

            "uniform sampler2D _color_tex;\n"
            "smooth in vec2 ex_texture;\n"
            "smooth in vec3 ex_normal;\n"

            "struct DirectionalLight\n"
            " {\n"
            "    vec3 vColor;\n"
            "    vec3 vDirection;\n"
            "    float fAmbient;\n"
            " };\n"


            " vec4 GetDirectionalLightColor(DirectionalLight dirLight, vec3 vNormal)\n"
            "  {\n"
            "      float fDiffuseIntensity = max(0.0, dot(vNormal, -dirLight.vDirection));\n"
            "      float fMult = clamp(dirLight.fAmbient+fDiffuseIntensity, 0.0, 1.0);\n"
            "       return vec4(dirLight.vColor*fMult, 1.0);\n"
            "  }\n"

           " DirectionalLight light= DirectionalLight(vec3(1.0,1.0,1.0),vec3(0.0,0.0,-1.0),0.6); \n"


            "\n"
            "void main(void) {\n"
            "\n"
            " vec4 vColor = vec4(1.0,1.0,1.0,1.0);\n"  //white
            " vec3 vNormalized = normalize(ex_normal);\n"
            " vec4 vTexColor = texture(_color_tex, ex_texture);\n"

            " vec4 vMixedColor = vTexColor;\n"
            " vec4 vDirLightColor = GetDirectionalLightColor(light, vNormalized);\n"

            " gl_FragColor = vMixedColor*vDirLightColor;\n"

            "}\n"
            ;



    bool compile_ok, link_ok;




    GMlib::GL::VertexShader vshader;
    vshader.create("texture_vs2");
    vshader.setPersistent(true);
    vshader.setSource(vs_src);
    compile_ok = vshader.compile();

    if( !compile_ok ) {
        std::cout << "Src:" << std::endl << vshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << vshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    GMlib::GL::FragmentShader fshader;
    fshader.create("texture_fs2");
    fshader.setPersistent(true);
    fshader.setSource(fs_src);
    compile_ok = fshader.compile();
    if( !compile_ok ) {
        std::cout << "Src:" << std::endl << fshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << fshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    _prog.create(prog_name);
    _prog.setPersistent(true);
    _prog.attachShader(vshader);
    _prog.attachShader(fshader);
    link_ok = _prog.link();
    assert(link_ok);
}




