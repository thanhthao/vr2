
/**********************************************************************************
**
** Copyright (C) 1994 Narvik University College
** Contact: GMlib Online Portal at http://episteme.hin.no
**
** This file is part of the Geometric Modeling Library, GMlib.
**
** GMlib is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** GMlib is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with GMlib.  If not, see <http://www.gnu.org/licenses/>.
**
**********************************************************************************/
#include "vis_particles.h"

// gmlib
#include <opengl/gmopengl.h>
#include <opengl/gmopenglmanager.h>
#include <scene/gmscene.h>
#include <scene/camera/gmcamera.h>
#include <scene/light/gmlight.h>
#include <scene/utils/gmmaterial.h>
#include <parametrics/gmpsurf.h>
// stl
#include <set>
#include <string>
#include <iostream>
#include <cstdlib>
#include <ctime>

#define number_particle 1000


VisParticles::VisParticles()
    : _no_strips(0), _no_strip_indices(0), _strip_size(0)
{

    time         = 0.0;
    cycletime    = 3.0;
    acceleration = -0.981;
    vel.resize(number_particle);

    for(int i = 0; i < vel.size();i++)
    {
        vel[i].x = (float) (rand() % 5);
        vel[i].y = (float) (rand() % 5 );
        vel[i].z = (float) (rand() % 5);
        vel[i].w = (float) (cycletime*(rand() % 5));
       // std::cout<<"vel : "<<vel[i].x<<" "<<vel[i].y<<" "<<vel[i].z<<" "<<vel[i].w<<std::endl;
    }

    initShaderProgram();

    _vbo2.create();
    _vbo2.bufferData(number_particle*sizeof(Velocity),vel.data(),GL_STATIC_DRAW);

}


VisParticles::VisParticles(const VisParticles &copy)
    :PSurfVisualizer<float,3>(copy), _no_strips(0), _no_strip_indices(0), _strip_size(0)
{

    initShaderProgram();
    _color_prog.acquire("color");
    assert(_color_prog.isValid());
    _vbo.create();

}



void VisParticles::render( const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer ) const {

    const GMlib::Camera* cam = renderer->getCamera();
    const GMlib::HqMatrix<float,3> &mvmat = obj->getModelViewMatrix(cam);
    const GMlib::HqMatrix<float,3> &pmat = obj->getProjectionMatrix(cam);
    //  const SqMatrix<float,3> &nmat = obj->getNormalMatrix(cam);


    this->glSetDisplayMode();

    _prog.bind(); {

        _prog.setUniform( "u_mvpmat", pmat * mvmat );

        time += 0.0000000000001;
        _prog.setUniform( "time", time );
        _prog.setUniform( "cycletime", cycletime );
        _prog.setUniform( "acceleration", acceleration);


        GMlib::GL::AttributeLocation vel_loc = _prog.getAttributeLocation( "in_velocity" );

        _vbo2.bind();
        _vbo2.enable( vel_loc,  4, GL_FLOAT, GL_FALSE, sizeof(float)*4, reinterpret_cast<const GLvoid *>(0x0) );

                                                                   // *4 vi x,y,z,w
        draw();

        _vbo2.disable( vel_loc );
        _vbo2.unbind();

    } _prog.unbind();
}



void VisParticles::draw() const {

    glDrawArrays(GL_POINTS,0,number_particle)  ;
}



void VisParticles::replot(
        const GMlib::DMatrix< GMlib::DMatrix< GMlib::Vector<float, 3> > >& p,
        const GMlib::DMatrix< GMlib::Vector<float, 3> >& normals,
        int /*m1*/, int /*m2*/, int /*d1*/, int /*d2*/,
        bool closed_u, bool closed_v  ) {


}



void VisParticles::renderGeometry( const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color ) const {


}


void VisParticles::initShaderProgram() {

    const std::string prog_name    = "particle_prog_p";
    if( _prog.acquire(prog_name) ) return;


    std::string vs_src =
            GMlib::GL::OpenGLManager::glslDefHeader150Source() +

            "uniform mat4 u_mvpmat;\n"
            " uniform float acceleration;\n"
            " uniform float time;\n"
            " uniform float cycleTime;\n"
            " in vec4 in_velocity;\n"
            "vec4 position;\n"

            "\n"
            "void main(void) {\n"
            "\n"
            "   if(time > in_velocity.w)\n"
            "{\n"
            "   float t = time - in_velocity.w;\n"
           // "   t = mod(t,cycleTime);\n"
            "   t = 0.0000000001;\n"
            "  position = vec4(in_velocity.x*t,in_velocity.y*t + 0.5*acceleration*t*t,in_velocity.z*t ,1.0);\n"

            "}\n"

            "  gl_Position = u_mvpmat * position;"
            "}\n"
            ;




    std::string fs_src =
            GMlib::GL::OpenGLManager::glslDefHeader150Source() +
            GMlib::GL::OpenGLManager::glslFnComputeLightingSource() +

            "void main(void) {\n"
            "\n"
            "  gl_FragColor = vec4 (1.0, 1.0, 1.0, 1.0);\n"
            "}\n"
            ;


    bool compile_ok, link_ok;

    GMlib::GL::VertexShader vshader;
    vshader.create("particle_vs");
    vshader.setPersistent(true);
    vshader.setSource(vs_src);
    compile_ok = vshader.compile();
    if( !compile_ok ) {
        std::cout << "Src:" << std::endl << vshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << vshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    GMlib::GL::FragmentShader fshader;
    fshader.create("particle_fs");
    fshader.setPersistent(true);
    fshader.setSource(fs_src);
    compile_ok = fshader.compile();
    if( !compile_ok ) {
        std::cout << "Src:" << std::endl << fshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << fshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    _prog.create(prog_name);
    _prog.setPersistent(true);
    _prog.attachShader(vshader);
    _prog.attachShader(fshader);
    link_ok = _prog.link();
    assert(link_ok);
}






