
/**********************************************************************************
**
** Copyright (C) 1994 Narvik University College
** Contact: GMlib Online Portal at http://episteme.hin.no
**
** This file is part of the Geometric Modeling Library, GMlib.
**
** GMlib is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** GMlib is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with GMlib.  If not, see <http://www.gnu.org/licenses/>.
**
**********************************************************************************/
#include "vis_particles2.h"
#include <SOIL.h>
// gmlib
#include <opengl/gmopengl.h>
#include <opengl/gmopenglmanager.h>
#include <scene/gmscene.h>
#include <scene/camera/gmcamera.h>
#include <scene/light/gmlight.h>
#include <scene/utils/gmmaterial.h>
#include <parametrics/gmpsurf.h>
#include <opengl/gmgeometryshader>
// stl
#include <set>
#include <string>
#include <iostream>
#include <cstdlib>

#define number_particle  128*1024


VisParticles2::VisParticles2()
    : _no_strips(0), _no_strip_indices(0), _strip_size(0)
{


    _vertexData.resize(number_particle*3);

    for(int i = 0;i<number_particle;++i)
       {
           int arm = 3*(std::rand()/float(RAND_MAX));
           float alpha = 1/(0.1f+std::pow(std::rand()/float(RAND_MAX),0.7f))-1/1.1f;
           float r = 4.0f*alpha;
           alpha += arm*2.0f*3.1416f/3.0f;

           _vertexData[3*i+0] = r*std::sin(alpha);
           _vertexData[3*i+1] = 0;
           _vertexData[3*i+2] = r*std::cos(alpha);

           _vertexData[3*i+0] += (4.0f-0.2*alpha)*(2-(std::rand()/float(RAND_MAX)+std::rand()/float(RAND_MAX)+
                                                     std::rand()/float(RAND_MAX)+std::rand()/float(RAND_MAX)));
           _vertexData[3*i+1] += (2.0f-0.1*alpha)*(2-(std::rand()/float(RAND_MAX)+std::rand()/float(RAND_MAX)+
                                                     std::rand()/float(RAND_MAX)+std::rand()/float(RAND_MAX)));
           _vertexData[3*i+2] += (4.0f-0.2*alpha)*(2-(std::rand()/float(RAND_MAX)+std::rand()/float(RAND_MAX)+
                                                     std::rand()/float(RAND_MAX)+std::rand()/float(RAND_MAX)));
       }


    initShaderProgram();

    _vbo.create();

    _vbo.bufferData(sizeof(GLfloat)*_vertexData.size(),&_vertexData[0],GL_STATIC_DRAW);

}


VisParticles2::VisParticles2(const VisParticles2 &copy)
    :PSurfVisualizer<float,3>(copy), _no_strips(0), _no_strip_indices(0), _strip_size(0)
{

    initShaderProgram();
    _vbo.create();

}



void VisParticles2::render( const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer ) const {

    const GMlib::Camera* cam = renderer->getCamera();
    const GMlib::HqMatrix<float,3> &mvmat = obj->getModelViewMatrix(cam);
    const GMlib::HqMatrix<float,3> &pmat = obj->getProjectionMatrix(cam);
    //  const SqMatrix<float,3> &nmat = obj->getNormalMatrix(cam);


    this->glSetDisplayMode();

    _prog.bind(); {

        // Model view and projection matrices
        _prog.setUniform( "u_mvpmat", pmat * mvmat );


        // Get vertex and texture attrib locations
        GMlib::GL::AttributeLocation vert_loc = _prog.getAttributeLocation( "in_vertex" );

        // Bind and draw
        _vbo.bind();
        _vbo.enable( vert_loc, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), (char*)0 + 0*sizeof(GLfloat));

        draw();


        _vbo.disable( vert_loc );
        _vbo.unbind();

    } _prog.unbind();
}



void VisParticles2::draw() const {

    glDrawArrays(GL_POINTS,0,number_particle)  ;
}



void VisParticles2::replot(
        const GMlib::DMatrix< GMlib::DMatrix< GMlib::Vector<float, 3> > >& p,
        const GMlib::DMatrix< GMlib::Vector<float, 3> >& normals,
        int /*m1*/, int /*m2*/, int /*d1*/, int /*d2*/,
        bool closed_u, bool closed_v  ) {


}



void VisParticles2::renderGeometry( const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color ) const {

}


void VisParticles2::initShaderProgram() {

    const std::string prog_name    = "particle_prog_2";
    if( _prog.acquire(prog_name) ) return;


    std::string vs_src =
            GMlib::GL::OpenGLManager::glslDefHeader150Source() +

            "uniform mat4 u_mvpmat;\n"
            "in  vec4 in_vertex;\n"
            "void main(void) {\n"
            "\n"
            "  gl_Position = u_mvpmat * in_vertex;"
            "}\n"
            ;

    std::string gs_src =
            GMlib::GL::OpenGLManager::glslDefHeader150Source() +

            "uniform mat4 u_mvpmat;\n"
            "  layout(points) in;\n"
            "  layout(triangle_strip, max_vertices=4) out;\n"

            "out vec2 txcoord;\n"
                    "void main() {\n"
                    "   vec4 pos = gl_in[0].gl_Position;\n"
                    "   txcoord = vec2(-1.0,-1.0);\n"
                    "   gl_Position = u_mvpmat*(pos+vec4(txcoord,0.0,0.0));\n"
                    "   EmitVertex();\n"
                    "   txcoord = vec2( 1.0,-1.0);\n"
                    "   gl_Position = u_mvpmat*(pos+vec4(txcoord,0.0,0.0));\n"
                    "   EmitVertex();\n"
                    "   txcoord = vec2(-1.0, 1.0);\n"
                    "   gl_Position = u_mvpmat*(pos+vec4(txcoord,0.0,0.0));\n"
                    "   EmitVertex();\n"
                    "   txcoord = vec2( 1.0, 1.0);\n"
                    "   gl_Position = u_mvpmat*(pos+vec4(txcoord,0.0,0.0));\n"
                    "   EmitVertex();\n"
                    "EndPrimitive();\n"
                    "}\n"

            ;

    std::string fs_src =
            GMlib::GL::OpenGLManager::glslDefHeader150Source() +
            GMlib::GL::OpenGLManager::glslFnComputeLightingSource() +
                   "in vec2 txcoord;\n"
                   "void main() {\n"
                   "   float s = 0.2*(1.0/(1.0+15.0*dot(txcoord, txcoord))-1/16.0);\n"
                   "   gl_FragColor = s*vec4(1.0,0.9,0.6,1.0);\n"
                   "}\n";


    bool compile_ok, link_ok;

    GMlib::GL::VertexShader vshader;
    vshader.create("particle_vs2");
    vshader.setPersistent(true);
    vshader.setSource(vs_src);
    compile_ok = vshader.compile();
    if( !compile_ok ) {
        std::cout << "Src:" << std::endl << vshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << vshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    GMlib::GL::GeometryShader gshader;
    gshader.create("gs_particle2");
    gshader.setPersistent(true);
    gshader.setSource(gs_src);
    compile_ok = gshader.compile();
    if( !compile_ok ) {
        std::cout << "Src:" << std::endl << gshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << gshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    GMlib::GL::FragmentShader fshader;
    fshader.create("particle_fs2");
    fshader.setPersistent(true);
    fshader.setSource(fs_src);
    compile_ok = fshader.compile();
    if( !compile_ok ) {
        std::cout << "Src:" << std::endl << fshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << fshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    _prog.create(prog_name);
    _prog.setPersistent(true);
    _prog.attachShader(vshader);
    _prog.attachShader(gshader);
    _prog.attachShader(fshader);
    link_ok = _prog.link();
    assert(link_ok);
}


