#ifndef __2PARTICLES_H__
#define __2PARTICLES_H__

// gmlib
#include <opengl/bufferobjects/gmvertexbufferobject.h>
#include <opengl/bufferobjects/gmindexbufferobject.h>
#include <opengl/bufferobjects/gmuniformbufferobject.h>
#include <opengl/gmtexture.h>
#include <opengl/gmprogram.h>
#include <opengl/shaders/gmvertexshader.h>
#include <opengl/shaders/gmfragmentshader.h>
#include <scene/render/gmdefaultrenderer.h>


#include <QDebug>
#include "parametrics/visualizers/gmpsurfvisualizer.h"

//#include <parametrics/visualizers/gmpsurfdefaultvisualizer.h>


class VisParticles2 : public GMlib::PSurfVisualizer<float,3> {
               GM_VISUALIZER(VisParticles2)
public:
  VisParticles2();
  VisParticles2( const VisParticles2& copy );


  void          render( const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer ) const;
  void          renderGeometry( const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color ) const;

  virtual void  replot( const GMlib::DMatrix< GMlib::DMatrix< GMlib::Vector<float, 3> > >& p,
                        const GMlib::DMatrix< GMlib::Vector<float, 3> >& normals,
                        int m1, int m2, int d1, int d2,
                        bool closed_u, bool closed_v
  );
private:
  GMlib::GL::Program                 _prog;
  GMlib::GL::Program                 _color_prog;

  GMlib::GL::VertexBufferObject      _vbo,_vbo2;
  GMlib::GL::IndexBufferObject       _ibo;
  GMlib::GL::Texture                 _nmap;

  GLuint                             _no_strips;
  GLuint                             _no_strip_indices;
  GLsizei                            _strip_size;

  void                                 draw() const;

  void                               initShaderProgram();
// them
  GLuint                              _tex;
  std::vector<GLfloat>                _vertexData;
}; // END class



#endif // 2PARTICLES_H__
