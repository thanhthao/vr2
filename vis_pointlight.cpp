

#include "vis_pointlight.h"
#include <SOIL.h>
// gmlib
#include <opengl/gmopengl.h>
#include <opengl/gmopenglmanager.h>
#include <scene/gmscene.h>
#include <scene/camera/gmcamera.h>
#include <scene/light/gmlight.h>
#include <scene/utils/gmmaterial.h>
#include <parametrics/gmpsurf.h>
// stl
#include <set>
#include <string>


VisPointLight::VisPointLight()
    : _no_strips(0), _no_strip_indices(0), _strip_size(0)
{

    initShaderProgram();

    _color_prog.acquire("color");
    assert(_color_prog.isValid());

    _vbo.create();
    _vbo2.create();
    _ibo.create();



}

VisPointLight::VisPointLight(const VisPointLight &copy)
    :PSurfVisualizer<float,3>(copy), _no_strips(0), _no_strip_indices(0), _strip_size(0)
{

    initShaderProgram();

    _color_prog.acquire("color");
    assert(_color_prog.isValid());

    _vbo.create();
    _ibo.create();

}


void VisPointLight::render( const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer ) const {

    const GMlib::Camera* cam = renderer->getCamera();
    const GMlib::HqMatrix<float,3> &mvmat = obj->getModelViewMatrix(cam);
    const GMlib::HqMatrix<float,3> &pmat = obj->getProjectionMatrix(cam);

    GMlib::SqMatrix<float,3>        nmat = mvmat.getRotationMatrix();


    this->glSetDisplayMode();

    _prog.bind(); {
        _prog.setUniform( "u_mvmat", mvmat );
        _prog.setUniform( "u_mvpmat", pmat * mvmat );
        _prog.setUniform( "u_nmat", nmat );
        // Lights
        _prog.setUniformBlockBinding( "Lights", renderer->getLightUBO(), 0 );

        // Material
        const GMlib::Material &m = obj->getMaterial();
        _prog.setUniform( "u_mat_amb", m.getAmb() );
        _prog.setUniform( "u_mat_dif", m.getDif() );
        _prog.setUniform( "u_mat_spc", m.getSpc() );
        _prog.setUniform( "u_mat_shi", m.getShininess() );

        // Get vertex and texture attrib locations
        GMlib::GL::AttributeLocation vert_loc = _prog.getAttributeLocation( "in_vertex" );
        GMlib::GL::AttributeLocation tex_loc = _prog.getAttributeLocation( "in_texture" );

        GMlib::GL::AttributeLocation normal_loc = _prog.getAttributeLocation( "in_normal" );


        // Bind and draw
        _vbo.bind();
        _vbo.enable( vert_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );
        _vbo.enable( tex_loc,  2, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(3*sizeof(GLfloat)) );

        _vbo2.bind();
        _vbo2.enable( normal_loc,  3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLNormal), reinterpret_cast<const GLvoid *>(0x0) );
        //
        draw();
        _vbo2.disable( normal_loc );
        _vbo2.unbind();

        _vbo.disable( vert_loc );
        _vbo.disable( tex_loc );
        _vbo.unbind();

    } _prog.unbind();
}



void VisPointLight::draw() const {

    _ibo.bind();
    for( unsigned int i = 0; i < _no_strips; ++i )
        _ibo.drawElements( GL_TRIANGLE_STRIP, _no_strip_indices, GL_UNSIGNED_INT, reinterpret_cast<const GLvoid *>(i * _strip_size) );
    _ibo.unbind();
}



void VisPointLight::replot(
        const GMlib::DMatrix< GMlib::DMatrix< GMlib::Vector<float, 3> > >& p,
        const GMlib::DMatrix< GMlib::Vector<float, 3> >& normals,
        int /*m1*/, int /*m2*/, int /*d1*/, int /*d2*/,
        bool closed_u, bool closed_v  ) {

    PSurfVisualizer::fillStandardVBO( _vbo, p );
    PSurfVisualizer::fillTriangleStripIBO( _ibo, p.getDim1(), p.getDim2(), _no_strips, _no_strip_indices, _strip_size );

    getNormal(_vbo2,normals);
}

// make new one
void VisPointLight::getNormal(GMlib::GL::VertexBufferObject  &vbo,
                              const GMlib::DMatrix<GMlib::Vector<float, 3> >  &n) {

    GLsizeiptr no_vertices = n.getDim1() * n.getDim2() * sizeof(GMlib::GL::GLNormal);

    vbo.bufferData( no_vertices, 0x0, GL_STATIC_DRAW );
    GMlib::GL::GLNormal *ptr = vbo.mapBuffer<GMlib::GL::GLNormal>();  //glnormal
    for( int i = 0; i < n.getDim1(); i++ ) {
        for( int j = 0; j < n.getDim2(); j++ ) {

            ptr->nx = n(i)(j)(0);
            ptr->ny = n(i)(j)(1);
            ptr->nz = n(i)(j)(2);

            ptr++;
        }
    }
    vbo.unmapBuffer();
}



void VisPointLight::renderGeometry( const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color ) const {

    _color_prog.bind(); {
        _color_prog.setUniform( "u_color", color );
        _color_prog.setUniform( "u_mvpmat", obj->getModelViewProjectionMatrix(renderer->getCamera()) );
        GMlib::GL::AttributeLocation vertice_loc = _color_prog.getAttributeLocation( "in_vertices" );

        _vbo.bind();
        _vbo.enable( vertice_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );

        draw();

        _vbo.disable( vertice_loc );
        _vbo.unbind();

    } _color_prog.unbind();
}


void VisPointLight::initShaderProgram() {

    const std::string prog_name    = "lighting_prog2";
    if( _prog.acquire(prog_name) ) return;


    std::string vs_src =
            GMlib::GL::OpenGLManager::glslDefHeader150Source() +

            "in vec4 in_vertex,in_normal;\n"
            "uniform mat4 u_mvpmat, u_mvmat;\n"
            "uniform mat3 u_nmat;\n"

            "out vec3 ex_normal;\n"
            "out vec3 ex_vertex;\n"
            "out vec3 lightDir;\n"
            "out vec3 eye;\n"

            "\n"
            "void main(void) {\n"
            "\n"
            "vec3 light_pos = vec3(0.0,-1.0,0.0) ; \n"

            " ex_vertex = vec3(u_mvmat*in_vertex) ;\n "
            " ex_normal = normalize(u_nmat * vec3(in_normal));\n "
            " lightDir = vec3(light_pos - ex_vertex); \n "
            " eye = vec3(- ex_vertex);\n "

            "  gl_Position = u_mvpmat * in_vertex;"
            "}\n"
            ;


    std::string fs_src =
            GMlib::GL::OpenGLManager::glslDefHeader150Source() +
            GMlib::GL::OpenGLManager::glslFnComputeLightingSource() +


            "in vec3 ex_normal;\n"
            "in vec3 lightDir;\n"
            "in vec3 eye;\n"

            "struct Materials \n"
            " { \n"
            " vec4 diffuse; \n"
            " vec4 ambient; \n"
            " vec4 specular; \n"
            " float shininess; \n"
            " }; \n"

            " Materials material = Materials(vec4(0.9,0.5,0.5,1.0),vec4(0.3,0.3,0.3,1.0),vec4(0.6,0.6,0.6,1.0),32.0); \n"

            "void main(void) {\n"
            "\n"
            "   vec4 spec = vec4(0.0,0.0,0.0,0.0);\n"
            "   vec3 n = normalize(ex_normal);\n"
            "   vec3 l = normalize(lightDir);\n"
            "   vec3 e = normalize(eye);\n"

            "   float intensity = max(dot(n,l), 0.0);\n"
            "   if (intensity > 0.0) { \n"
            "       vec3 h = normalize(l + e); \n"
            "       float intSpec = max(dot(h,n), 0.0); \n"
            "       spec = material.specular * pow(intSpec, material.shininess); \n"
            "    } \n"

            "   gl_FragColor= max(intensity * material.diffuse + spec, material.ambient);; \n"
            "  } \n"

            ;



    bool compile_ok, link_ok;

    GMlib::GL::VertexShader vshader;
    vshader.create("light_vs2");
    vshader.setPersistent(true);
    vshader.setSource(vs_src);
    compile_ok = vshader.compile();


    if( !compile_ok ) {
        std::cout << "Src:" << std::endl << vshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << vshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);


    GMlib::GL::FragmentShader fshader;
    fshader.create("light_fs2");
    fshader.setPersistent(true);
    fshader.setSource(fs_src);
    compile_ok = fshader.compile();
    if( !compile_ok ) {
        std::cout << "Src:" << std::endl << fshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << fshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    _prog.create(prog_name);
    _prog.setPersistent(true);
    _prog.attachShader(vshader);
    _prog.attachShader(fshader);
    link_ok = _prog.link();
    assert(link_ok);
}

