
/**********************************************************************************
**
** Copyright (C) 1994 Narvik University College
** Contact: GMlib Online Portal at http://episteme.hin.no
**
** This file is part of the Geometric Modeling Library, GMlib.
**
** GMlib is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** GMlib is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public License
** along with GMlib.  If not, see <http://www.gnu.org/licenses/>.
**
**********************************************************************************/
#include "visualizer.h"

// gmlib
#include <opengl/gmopengl.h>
#include <opengl/gmopenglmanager.h>
#include <scene/gmscene.h>
#include <scene/camera/gmcamera.h>
#include <scene/light/gmlight.h>
#include <scene/utils/gmmaterial.h>
#include <parametrics/gmpsurf.h>
// stl
#include <set>
#include <string>


  Visualization::Visualization()
   : _no_strips(0), _no_strip_indices(0), _strip_size(0)
  {

    initShaderProgram();

    _color_prog.acquire("color");
    assert(_color_prog.isValid());

    _vbo.create();
    _ibo.create();
    _nmap.create(GL_TEXTURE_2D);
  }


  Visualization::Visualization(const Visualization &copy)
    :PSurfVisualizer<float,3>(copy), _no_strips(0), _no_strip_indices(0), _strip_size(0)
  {

    initShaderProgram();

    _color_prog.acquire("color");
    assert(_color_prog.isValid());


    _vbo.create();
    _ibo.create();
    _nmap.create(GL_TEXTURE_2D);
  }



  void Visualization::render( const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer ) const {

    const GMlib::Camera* cam = renderer->getCamera();
    const GMlib::HqMatrix<float,3> &mvmat = obj->getModelViewMatrix(cam);
    const GMlib::HqMatrix<float,3> &pmat = obj->getProjectionMatrix(cam);
//  const SqMatrix<float,3> &nmat = obj->getNormalMatrix(cam);

    GMlib::SqMatrix<float,3>        nmat = mvmat.getRotationMatrix();
    nmat.invertOrthoNormal();
    nmat.transpose();

    this->glSetDisplayMode();

    _prog.bind(); {

      // Model view and projection matrices
//      _prog.setUniform( "u_mvmat", mvmat );
        _prog.setUniform( "u_mvpmat", pmat * mvmat );
//      _prog.setUniform( "u_nmat", nmat );

//      // Lights
     _prog.setUniformBlockBinding( "Lights", renderer->getLightUBO(), 0 );

      // Get vertex and texture attrib locations
      GMlib::GL::AttributeLocation vert_loc = _prog.getAttributeLocation( "in_vertex" );
    //  GMlib::GL::AttributeLocation tex_loc = _prog.getAttributeLocation( "in_tex" );

      // Bind and draw
      _vbo.bind();
      _vbo.enable( vert_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );

      draw();

      _vbo.disable( vert_loc );
     // _vbo.disable( tex_loc );
      _vbo.unbind();

    } _prog.unbind();
  }



  void Visualization::draw() const {

    _ibo.bind();
    for( unsigned int i = 0; i < _no_strips; ++i )
      _ibo.drawElements( GL_TRIANGLE_STRIP, _no_strip_indices, GL_UNSIGNED_INT, reinterpret_cast<const GLvoid *>(i * _strip_size) );
    _ibo.unbind();
  }



  void Visualization::replot(
          const GMlib::DMatrix< GMlib::DMatrix< GMlib::Vector<float, 3> > >& p,
                                  const GMlib::DMatrix< GMlib::Vector<float, 3> >& normals,
                                 int /*m1*/, int /*m2*/, int /*d1*/, int /*d2*/,
                                  bool closed_u, bool closed_v  ) {

    PSurfVisualizer::fillStandardVBO( _vbo, p );
    PSurfVisualizer::fillTriangleStripIBO( _ibo, p.getDim1(), p.getDim2(), _no_strips, _no_strip_indices, _strip_size );
    PSurfVisualizer::fillNMap( _nmap, normals, closed_u, closed_v );
  }



  void Visualization::renderGeometry( const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color ) const {

    _color_prog.bind(); {
      _color_prog.setUniform( "u_color", color );
     _color_prog.setUniform( "u_mvpmat", obj->getModelViewProjectionMatrix(renderer->getCamera()) );
      GMlib::GL::AttributeLocation vertice_loc = _color_prog.getAttributeLocation( "in_vertex" );

      _vbo.bind();
      _vbo.enable( vertice_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );

      draw();

      _vbo.disable( vertice_loc );
      _vbo.unbind();

    } _color_prog.unbind();
  }


  void Visualization::initShaderProgram() {

    const std::string prog_name    = "psurf_VD_prog";
    if( _prog.acquire(prog_name) ) return;



    std::string vs_src =
        GMlib::GL::OpenGLManager::glslDefHeader150Source() +

                     "uniform mat4 u_mvpmat;\n"
                     "in vec4 in_vertex;\n"
                     "\n"
                     "void main(void) {\n"
                     "\n"
                     "  gl_Position = u_mvpmat * in_vertex;"
                     "}\n"
                     ;

    std::string fs_src =
        GMlib::GL::OpenGLManager::glslDefHeader150Source() +
        GMlib::GL::OpenGLManager::glslFnComputeLightingSource() +

            "void main(void) {\n"
                     "\n"
                     "  gl_FragColor = vec4 (0.0, 1.0, 0.0, 0.0);\n"
                     "}\n"
                     ;


    bool compile_ok, link_ok;

    GMlib::GL::VertexShader vshader;
    vshader.create("psurf_VD_vs");
    vshader.setPersistent(true);
    vshader.setSource(vs_src);
    compile_ok = vshader.compile();
    assert(compile_ok);

    GMlib::GL::FragmentShader fshader;
    fshader.create("psurf_VD_fs");
    fshader.setPersistent(true);
    fshader.setSource(fs_src);
    compile_ok = fshader.compile();
    if( !compile_ok ) {
      std::cout << "Src:" << std::endl << fshader.getSource() << std::endl << std::endl;
      std::cout << "Error: " << fshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    _prog.create(prog_name);
    _prog.setPersistent(true);
    _prog.attachShader(vshader);
    _prog.attachShader(fshader);
    link_ok = _prog.link();
    assert(link_ok);
  }






